use super::*;

#[derive(Debug)]
pub enum DbgSource{
    String{
        result: u32,
        string: String,
    },
    SourceExtension(String),
    Source{
        language: SourceLanguage,
        version: u32,
        file: Option<u32>,
        source: Option<String>
    },
    SourceContinued(String),
}

named!(parse_dbg_source_continued<&[u8], DbgSource>,
    preceded!(call!(tag_u16,2),
    length_value!(map!(le_u16,|x| (x - 1) * 4),
        do_parse!(
            string: map_res!(take_until!("\0"),str::from_utf8) >>
            (DbgSource::SourceContinued( string.to_string())))
        )
    )
    );

named!(parse_dbg_source_source<&[u8], DbgSource>,
    preceded!(call!(tag_u16,3),
    length_value!(map!(le_u16,|x| (x - 1) * 4),
        do_parse!(
            language: map_opt!(le_u32,SourceLanguage::from_u32) >>
            version: le_u32 >>
            len: call!(length) >>
            file: cond!(len != 0,le_u32) >>
            len: call!(length) >>
            source: cond!(len != 0,map_res!(take_until!("\0"),str::from_utf8)) >>
            (DbgSource::Source{
                language,
                version,
                file,
                source: source.map(|x| x.to_string()),
            }))
        )
    )
    );

named!(parse_dbg_source_extension<&[u8], DbgSource>,
    preceded!(call!(tag_u16,4),
    length_value!(map!(le_u16,|x| (x - 1) * 4),
        do_parse!(
            string: map_res!(take_until!("\0"),str::from_utf8) >>
            (DbgSource::SourceExtension( string.to_string())))
        )
    )
    );

named!(parse_dbg_source_string<&[u8], DbgSource>,
    preceded!(call!(tag_u16,7),
    length_value!(map!(le_u16,|x| (x - 1) * 4),
        do_parse!(
            result: le_u32 >>
            string: map_res!(take_until!("\0"),str::from_utf8) >>
            (DbgSource::String{
                result,
                string: string.to_string(),
            }))
        )
    )
    );

named!(pub parse_dbg_source<&[u8], DbgSource>,
    alt!(
        parse_dbg_source_continued |
        parse_dbg_source_source |
        parse_dbg_source_extension |
        parse_dbg_source_string 
        )
    );

#[derive(Debug)]
pub enum DbgName{
    Name{
        target: u32,
        name: String,
    },
    MemberName{
        ty: u32,
        member: u32,
        name: String,
    },
}

named!(parse_dbg_name_name<&[u8], DbgName>,
    preceded!(call!(tag_u16, 5),
        length_value!(map!(le_u16,|x| (x -1) * 4),
            do_parse!(
                target: le_u32 >>
                name: map_res!(take_until!("\0"),str::from_utf8) >>
                (DbgName::Name{
                    target,
                    name: name.to_string(),
                })
                )
            )
        )
    );

named!(parse_dbg_name_member_name<&[u8], DbgName>,
    preceded!(call!(tag_u16, 6),
        length_value!(map!(le_u16,|x| (x -1) * 4),
            do_parse!(
                ty: le_u32 >>
                member: le_u32 >>
                name: map_res!(take_until!("\0"),str::from_utf8) >>
                (DbgName::MemberName{
                    ty,
                    member,
                    name: name.to_string(),
                }))
            )
        )
    );

named!(pub parse_dbg_name<&[u8], DbgName>,
    alt!(
        parse_dbg_name_name |
        parse_dbg_name_member_name 
        )
    );
