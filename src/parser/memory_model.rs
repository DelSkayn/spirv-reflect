use super::*;

#[derive(Debug)]
pub struct MemoryModel{
    addressing_model: AddressingModel,
    memory_model: SpirvMemoryModel,
}

named!(pub parse_memory_model<&[u8],MemoryModel>,
       do_parse!(
           call!(tag_u16,14) >>
           call!(tag_u16,3) >>
           addressing_model: map_opt!(le_u32, AddressingModel::from_u32) >>
           memory_model: map_opt!(le_u32, SpirvMemoryModel::from_u32) >>
           (MemoryModel{
               addressing_model,
               memory_model,
           })
           )
       );
