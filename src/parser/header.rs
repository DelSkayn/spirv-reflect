//TODO sort imports
use super::*;

#[derive(Debug)]
pub struct Version{
    major: u8,
    minor: u8,
}

named!(parse_version<&[u8], Version>,
       do_parse!(
           tag!([0]) >>
           minor: le_u8 >>
           major: le_u8 >>
           tag!([0]) >>
           (Version{
               major,
               minor,
           })
       )
);

#[derive(Debug)]
pub struct Header{
    version: Version,
    tool_number: u32,
    bound: u32,
}

named!(pub parse_header<&[u8], Header>, 
       do_parse!(
           tag!([0x03,0x02,0x23,0x07]) >>
           version: parse_version >>
           tool_number: le_u32 >>
           bound: le_u32 >>
           take!(4) >>
           (Header{
               version,
               tool_number,
               bound,
           })
       )
);
