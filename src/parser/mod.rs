use nom::*;
use num_traits::FromPrimitive;

use crate::spirv::{
    Capability,
    MemoryModel as SpirvMemoryModel,
    AddressingModel,
    ExecutionModel,
    ExecutionMode as SpirvExecutionMode,
    SourceLanguage,
    Decoration,
};

use std::{mem,str};

mod header;
use self::header::{
    Header,
    parse_header,
};

mod extension;
use self::extension::{
    Extension,
    ExtInstImport,
    parse_extension,
    parse_ext_inst_import,
};

mod memory_model;
use self::memory_model::{
    MemoryModel,
    parse_memory_model,
};

mod entry_point;
use self::entry_point::{
    EntryPoint,
    ExecutionMode,
    parse_entry_point,
    parse_execution_mode,
};

mod dbg;
use self::dbg::{
    DbgSource,
    DbgName,
    parse_dbg_source,
    parse_dbg_name,
};

mod decorate;
use self::decorate::{
    Decorate,
    parse_decoration,
};

fn tag_u32(i: &[u8],val: u32) -> IResult<&[u8],u32>{
    let (remaining,buf) = take!(i,4)?;
    let buf_val: [u8; 4] = unsafe{ mem::transmute(val)};
    if buf_val != buf{
        let e = ErrorKind::Tag;
        Err(Err::Error(Context::Code(i,e)))
    }else{
        Ok((remaining,val))
    }
}

fn tag_u16(i: &[u8],val: u16) -> IResult<&[u8],u16>{
    let (remaining,buf) = take!(i,2)?;
    let buf_val: [u8; 2] = unsafe{ mem::transmute(val)};
    if buf_val != buf{
        let e = ErrorKind::Tag;
        Err(Err::Error(Context::Code(i,e)))
    }else{
        Ok((remaining,val))
    }
}

fn print_remaining(i: &[u8]) -> IResult<&[u8],()>{
    println!("remaining: {:?}",i);
    Ok((i,()))
}

fn length(i: &[u8]) -> IResult<&[u8], usize>{
    Ok((i,i.len()))
}

named!(parse_string<&[u8], String>,
    map_res!(
        do_parse!(
            string: take_until!("\0") >>
            take!(if string.len() + 1 % 4 != 0{
                let res = 4 - ((string.len() + 1) % 4);
                println!("{}",res);
                // 1 extra for the \0 char
                res + 1
            }else{
                // 1 for the \0 char
                1
            }) >>
            (string))
        ,|x| str::from_utf8(x).map(|x| x.to_string()))
);


named_args!(parse_op(tag: u16)<&[u8],&[u8]>,
    preceded!(call!(tag_u16, tag),
        length_bytes!(map!(le_u16,|x| (x - 1) * 4))
    )
);

named_args!(parse_op_sized(tag: u16, size: u16)<&[u8],&[u8]>,
    preceded!(call!(tag_u16, tag),
        preceded!(call!(tag_u16,size),
            length_bytes!(map!(call!(tag_u16,size),|x| (x - 1) * 4))
        )
    )
);

named!(consume_op<&[u8],&[u8]>,
       do_parse!(
           take!(2) >>
           res: length_bytes!(map!(le_u16, |x| (x - 1) * 4)) >>
           (res)
       )
);

named!(parse_capability<&[u8], Capability>,
       map_opt!(
           do_parse!(
               call!(tag_u16,17) >>
               call!(tag_u16,2) >>
               capability: le_u32 >>
               (capability)
           ),
           Capability::from_u32
       )
);

#[derive(Debug)]
pub struct Spirv{
    header: Header,
    capabilities: Vec<Capability>,
    extensions: Vec<Extension>,
    extension_instruction_imports: Vec<ExtInstImport>,
    memory_model: MemoryModel,
    entry_points: Vec<EntryPoint>,
    execution_modes: Vec<ExecutionMode>,
    dbg_source: Vec<DbgSource>,
    dbg_name: Vec<DbgName>,
    decorations: Vec<Decorate>,
}

named!(parse_spirv<&[u8], Spirv>,
       do_parse!(
           header: parse_header >>
           capabilities: many0!(parse_capability) >>
           extensions: many0!(parse_extension) >>
           extension_instruction_imports: many0!(parse_ext_inst_import) >>
           memory_model: parse_memory_model >>
           entry_points: many0!(parse_entry_point) >>
           execution_modes: many0!(parse_execution_mode) >>
           dbg_source: many0!(parse_dbg_source) >>
           dbg_name: many0!(parse_dbg_name) >>
           decorations: many0!(parse_decoration) >>
           (Spirv{
               header,
               capabilities,
               extensions,
               extension_instruction_imports,
               memory_model,
               entry_points,
               execution_modes,
               dbg_source,
               dbg_name,
               decorations,
           })
)
);



pub fn parse(input: &[u8]) -> Result<Spirv,String>{
    let (_,res) = parse_spirv(input).unwrap();
    println!("{:#?}",res);
    Ok(res)
}

