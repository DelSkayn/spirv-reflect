use super::*;

#[derive(Debug)]
pub enum Decorate{
    Decorate{
        target: u32,
        decoration: Decoration,
    },
    MemberDecorate{
        structure_ty: u32,
        member: u32,
        decoration: Decoration,
    },
    GroupDecorate{
        id: u32,
        targets: Vec<u32>,
    },
    GroupMemberDecorate{
        id: u32,
        targets: Vec<(u32,u32)>,
    },
    DecorationGroup(u32),
}

named!(parse_decoration_decorate<&[u8],Decorate>,
    flat_map!(call!(parse_op,71),
        do_parse!(
            target: le_u32 >>
            len: call!(length) >>
            decoration: map_opt!(
                many_m_n!(len / 4, len / 4,le_u32),
                |x: Vec<u32>| Decoration::from_slice(x.as_slice())) >>
            (Decorate::Decorate{
                target,
                decoration,
            })
            )
        )
    );

named!(parse_decoration_member_decorate<&[u8],Decorate>,
    flat_map!(call!(parse_op,72),
        do_parse!(
            structure_ty: le_u32 >>
            member: le_u32 >>
            len: call!(length) >>
            decoration: map_opt!(
                many_m_n!(len / 4, len / 4,le_u32),
                |x: Vec<u32>| Decoration::from_slice(x.as_slice())) >>
            (Decorate::MemberDecorate{
                structure_ty,
                member,
                decoration,
            })
            )
        )
    );

named!(parse_decoration_group_decorate<&[u8],Decorate>,
    flat_map!(call!(parse_op,74),
        do_parse!(
            id: le_u32 >>
            len: call!(length) >>
            targets: many_m_n!(len / 4, len / 4,le_u32) >>
            (Decorate::GroupDecorate{
                id,
                targets,
            })
            )
        )
    );

named!(parse_decoration_group_member_decorate<&[u8],Decorate>,
    flat_map!(call!(parse_op,75),
        do_parse!(
            id: le_u32 >>
            len: call!(length) >>
            targets: many_m_n!(len / 8, len / 8,tuple!(le_u32,le_u32)) >>
            (Decorate::GroupMemberDecorate{
                id,
                targets,
            })
            )
        )
    );

named!(parse_decoration_group<&[u8],Decorate>,
    flat_map!(call!(parse_op,73),
        map!(le_u32,Decorate::DecorationGroup))
    );

named!(pub parse_decoration<&[u8], Decorate>,
       alt!(
           parse_decoration_decorate |
           parse_decoration_member_decorate |
           parse_decoration_group_decorate |
           parse_decoration_group_member_decorate |
           parse_decoration_group
           )
       );
