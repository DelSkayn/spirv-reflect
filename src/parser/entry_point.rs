use super::*;

#[derive(Debug)]
pub struct EntryPoint{
    execution_model: ExecutionModel,
    entry_point: u32,
    name: String,
    interfaces: Vec<u32>,
}


named!(pub parse_entry_point<&[u8], EntryPoint>,
   flat_map!(call!(parse_op,15),
       do_parse!(
           execution_model: map_opt!(le_u32, ExecutionModel::from_u32) >>
           entry_point: le_u32 >>
           name: parse_string >>
           length: call!(length) >>
           interfaces: many_m_n!(length / 4,length / 4,le_u32) >>
           (EntryPoint{
               execution_model,
               entry_point,
               name,
               interfaces,
           })
        )
    )
);

#[derive(Debug)]
pub struct ExecutionMode{
    entry_point: u32,
    mode: SpirvExecutionMode,
    literals: Vec<u32>,
}

named!(pub parse_execution_mode<&[u8], ExecutionMode>,
       flat_map!(call!(parse_op,16),
       do_parse!(
           entry_point: le_u32 >>
           mode: map_opt!(le_u32, SpirvExecutionMode::from_u32) >>
           length: call!(length) >>
           literals: many_m_n!(length / 4, length / 4, le_u32) >>
           (ExecutionMode{
               entry_point,
               mode,
               literals,
           })
       )
       )
);

