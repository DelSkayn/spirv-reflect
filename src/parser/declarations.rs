pub enum Declarations{
    TypeVoid{
        result: u32
    },
    TypeBool{
        result: u32,
    },
    TypeInt{
        result: u32,
        width: u32,
        signed: u32,
    },
    TypeFloat{
        result: u32,
        width: u32,
    },
    TypeVector{
        result: u32,
        component_ty: u32,
        count: u32,
    },
    TypeMatrix{
        result: u32,
        column_ty: u32,
        count: u32,
    },
    TypeImage{
        result: u32,
        sampled_ty: u32,
        dim: Dim,
        depth: u32,
        arrayed: u32,
        ms: u32,
        sampled: u32,
        format: ImageFormat,
        acces: Option<AccesQualifier>,
    },
    OpTypeSampler{
        result: u32,
    },
    OpTypeSampledImage{
        result: u32,
        image_ty: u32,
    },
    OpTypeArray{
        result: u32,
        element_ty: u32,
        length: u32,
    },
    OpTypeRuntimeArray{
        result: u32,
        element_ty: u32,
    },
    OpTypeStruct{
        result: u32,
        member_ty: Vec<u32>,
    },
    OpTypeOpaque{
        result: u32,
        name: String,
    },
    OpTypePointer{
        result: u32,
        storage_class: StorageClass,
        id: u32,
    }
}
