use super::*;

#[derive(Debug)]
pub struct Extension{
    name: String,
}

named!(pub parse_extension<&[u8], Extension>,
    flat_map!(call!(parse_op,10),
        map!(map_res!(take_until!("\0"), str::from_utf8),
             |x| Extension{ name: x.to_string() }))
    );

#[derive(Debug)]
pub struct ExtInstImport{
    result: u32,
    name: String,
}

named!(pub parse_ext_inst_import<&[u8], ExtInstImport>,
    flat_map!(call!(parse_op,11),
        do_parse!( 
            result: le_u32 >>
            name: map_res!(take_until!("\0"), str::from_utf8) >>
            (ExtInstImport{
              result,
              name: name.to_string(),
            })
            )
        )
    );
