#![allow(non_camel_case_types)]

use num_traits::FromPrimitive;

type LiteralNumber = u32;

/// Language of the source code of the spir-v binary.
#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum SourceLanguage{
    Unknown = 0,
    ESSL = 1,
    GLSL = 2,
    OpenCL_C = 3,
    OpenCL_CPP = 4,
    HLSL = 5,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum ExecutionModel{
    Vertex = 0,
    TessellationControl = 1,
    TesselationEvalutation = 2,
    Geometry = 3,
    Fragment = 4,
    GLCompute = 5,
    Kernel = 6,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum AddressingModel{
    Logical = 0,
    Physical32 = 1,
    Physical64 = 2,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum MemoryModel{
    Simple = 0,
    GLSL450 = 1,
    OpenCL = 2,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum ExecutionMode{
    Invocations = 0,
    SpacingEqual = 1,
    SpacingFractionalEven = 2,
    SpacingFractionalOdd = 3,
    VertexOrderCw = 4,
    VertexOrderCcw = 5,
    PixelCenterInteger = 6,
    OriginUpperLeft = 7,
    OriginLowerLeft = 8,
    EarlyFragmentTests = 9,
    PointMode = 10,
    Xfb = 11,
    DepthReplacing = 12,
    //weird jump here
    //Oh god it`s 13, those superstitious fucks.
    DepthGreater = 14,
    DepthLess = 15,
    DepthUnchanged = 16,
    LocalSize = 17,
    LocalSizeHint = 18,
    InputPoints = 19,
    InputLines = 20,
    InputLinesAdjacency = 21,
    Triangles = 22,
    InputTrianglesAdjacency = 23,
    Quads = 24,
    Isolines = 25,
    OutputVertices = 26,
    OutputPoints = 27,
    OutputLineStrip = 28,
    OutputTriangleStrip = 29,
    VecTypeHint = 30,
    ContractionOff = 31,
    PostDepthCoverage = 4446,
    StencilRefReplacingEXT = 5027,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum StorageClass{
    UniformConstant = 0,
    Input = 1,
    Uniform = 2,
    Output = 3,
    Workgroup = 4,
    CrossWorkgroup = 5,
    Private = 6,
    Function = 7,
    Generic = 8,
    PushConstant = 9,
    AtomicCounter = 10,
    Image = 11,
    StorageBuffer = 12,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum Dim{
    D1 = 0,
    D2 = 1,
    D3 = 2,
    Cube = 3,
    Rect = 4,
    Buffer = 5,
    SubpassData = 6
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum SamplerAddressingMode{
    None = 0,
    ClampToEdge = 1,
    Clamp = 2,
    Repeat = 3,
    RepeatMirrored = 4,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum SamplerFilterMode{
    Nearest = 0,
    Linear = 1,
}


#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum ImageFormat{
    Unkown = 0,
    Rgba32f = 1,
    Rgba16f = 2,
    R32f = 3,
    Rgba8 = 4,
    Rgba8Snorm = 5,
    Rg32f = 6,
    Rg16f = 7,
    R11fG11fB10f = 8,
    R16f = 9,
    Rgba16 = 10,
    Rgb10A2 = 11,
    Rg16 = 12,
    Rg8 = 13,
    R16 = 14,
    R8 = 15,
    Rgba16Snorm = 16,
    Rg16Snorm = 17,
    Rg8Snorm = 18,
    R16Snorm = 19,
    R8Snorm = 20,
    Rgba32i = 21,
    Rgba16i = 22,
    Rgba8i = 23,
    R32i = 24,
    Rg32i = 25,
    Rg16i = 26,
    Rg8i = 27,
    R16i = 28,
    R8i = 29,
    Rgba32ui = 30,
    Rgba16ui = 31,
    Rgba8ui = 32,
    R32ui = 33,
    Rgb10a2ui = 34,
    Rg32ui = 35,
    Rg16ui = 36,
    Rg8ui = 37,
    R16ui = 38,
    R8ui = 39,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum ImageChannelOrder{
    R = 0,
    A = 1,
    RG = 2,
    RA = 3,
    RGB = 4,
    RGBA = 5,
    BGRA = 6,
    ARGB = 7,
    Intensity = 8,
    Luminance = 9,
    Rx = 10,
    RGx = 11,
    RGBx = 12,
    Depth = 13,
    DepthStencil = 14,
    sRGB = 15,
    sRGBx = 16,
    sRGBA = 17,
    sBGRA = 18,
    ABGR = 19,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum ImageChannelDataType{
    SnormInt8 = 0,
    SnormInt16 = 1,
    UnormInt8 = 2,
    UnormInt16 = 3,
    UnormShort565 = 4,
    UnormShort555 = 5,
    UnormInt101010 = 6,
    SignedInt8 = 7,
    SignedInt16 = 8,
    SignedInt32 = 9,
    UnsignedInt8 = 10,
    UnsignedInt16 = 11,
    UnsignedInt32 = 12,
    HalfFloat = 13,
    Float = 14,
    UnormInt24 = 15,
    UnormInt101010_2 = 16,
}


#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum AccesQualifier{
    ReadOnly = 0,
    WriteOnly = 1,
    ReadWrite = 2,
}

#[derive(Debug)]
pub enum Decoration{
    RelaxedPrecision,
    SpecId(LiteralNumber),
    Block,
    BufferBlock,
    RowMajor,
    ColMajor,
    ArrayStride(LiteralNumber),
    MatrixStride(LiteralNumber),
    GLSLShared,
    GLSLPacked,
    CPacked,
    BuiltIn(BuiltIn),
    NoPerspective,
    Flat,
    Patch,
    Centroid,
    Sample,
    Invarient,
    Restrict,
    Aliased,
    Volatile,
    Constant,
    Coherent,
    NonWritable,
    NonReadable,
    Uniform,
    SaturatedConversion,
    Stream(LiteralNumber),
    Location(LiteralNumber),
    Component(LiteralNumber),
    Index(LiteralNumber),
    Binding(LiteralNumber),
    DescriptorSet(LiteralNumber),
    OffSet(LiteralNumber),
    XfbBuffer(LiteralNumber),
    XfbStride(LiteralNumber),
    FuncParamAttr(()),//TODO
    FPRoundingMode(()),//TODO
    FPFastMathMode(()),//TODO
    LinkageAttributes(String,()), //TODO
    NoContraction,
    InputAttachmentIndex(LiteralNumber),
    Alignment(LiteralNumber),
    ExplicitInterpAMD,
    OverrideCoverageNV,
    OassthroughNV,
    ViewportRelativeNV,
    SecondaryViewportRelativeNV(LiteralNumber),
}

impl Decoration{
    pub fn from_slice(s: &[u32]) -> Option<Self>{
        if s.len() < 1{
            return None
        }
        Some(match s[0] {
            0 => Decoration::RelaxedPrecision,
            1 => {
                if s.len() < 2{
                    return None
                }
                Decoration::SpecId(s[1])
            },
            2 => Decoration::Block,
            3 => Decoration::BufferBlock,
            4 => Decoration::RowMajor,
            5 => Decoration::ColMajor,
            6 => {
                if s.len() < 2{
                    return None
                }
                Decoration::ArrayStride(s[1])
            }
            7 => {
                if s.len() < 2{
                    return None
                }
                Decoration::MatrixStride(s[1])
            }
            8 => Decoration::GLSLShared,
            9 => Decoration::GLSLPacked,
            10 => Decoration::CPacked,
            11 => {
                if s.len() < 2{
                    return None
                }
                if let Some(x) = BuiltIn::from_u32(s[1]){
                    Decoration::BuiltIn(x)
                }else{
                    return None;
                }
            }
            13 => Decoration::NoPerspective,
            14 => Decoration::Flat,
            15 => Decoration::Patch,
            16 => Decoration::Centroid,
            17 => Decoration::Sample,
            18 => Decoration::Invarient,
            19 => Decoration::Restrict,
            20 => Decoration::Aliased,
            21 => Decoration::Volatile,
            22 => Decoration::Constant,
            23 => Decoration::Coherent,
            24 => Decoration::NonWritable,
            25 => Decoration::NonReadable,
            26 => Decoration::Uniform,
            28 => Decoration::SaturatedConversion,
            29 => {
                if s.len() < 2{
                    return None
                }
                Decoration::Stream(s[1])
            }
            30 => {
                if s.len() < 2{
                    return None
                }
                Decoration::Location(s[1])
            }
            31 => {
                if s.len() < 2{
                    return None
                }
                Decoration::Component(s[1])
            }
            32 => {
                if s.len() < 2{
                    return None
                }
                Decoration::Index(s[1])
            }
            33 => {
                if s.len() < 2{
                    return None
                }
                Decoration::Binding(s[1])
            }
            34 => {
                if s.len() < 2{
                    return None
                }
                Decoration::DescriptorSet(s[1])
            }
            35 => {
                if s.len() < 2{
                    return None
                }
                Decoration::OffSet(s[1])
            }
            36 => {
                if s.len() < 2{
                    return None
                }
                Decoration::XfbBuffer(s[1])
            }
            37 => {
                if s.len() < 2{
                    return None
                }
                Decoration::XfbStride(s[1])
            }
            38 => Decoration::FuncParamAttr(()),
            39 => Decoration::FPRoundingMode(()),
            40 => Decoration::FPFastMathMode(()),
            41 => unimplemented!(),
            42 => Decoration::NoContraction,
            43 => {
                if s.len() < 2{
                    return None
                }
                Decoration::InputAttachmentIndex(s[1])
            }
            44 => {
                if s.len() < 2{
                    return None
                }
                Decoration::Alignment(s[1])
            }
            4999 => Decoration::ExplicitInterpAMD,
            5249 => Decoration::OverrideCoverageNV,
            5250 => Decoration::OassthroughNV,
            5252 => Decoration::ViewportRelativeNV,
            5256 => {
                if s.len() < 2{
                    return None
                }
                Decoration::SecondaryViewportRelativeNV(s[1])
            }
            _ => return None,
        })
    }
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum BuiltIn{
    Position = 0,
    PointSize = 1,
    ClipDistance = 3,
    CullDistance = 4,
    VertexId = 5,
    InstanceId = 6,
    PrimitiveId = 7,
    InvocationId = 8,
    Layer = 9,
    ViewportIndex = 10,
    TessLevelOuter = 11,
    TessLevelInner = 12,
    TessCoord = 13,
    PatchVertices = 14,
    FragCoord = 15,
    PointCoord = 16,
    FrontFacing = 17,
    SampleId = 18,
    SamplePosition = 19,
    SampleMask = 20,
    FragDepth = 22,
    HelperInvocation = 23,
    NumWorkgroups = 24,
    WorkgroupSize = 25,
    WorkgroupId = 26,
    LocalInvocationId = 27,
    GlobalInvocationId = 28,
    LocalInvocationIndex = 29,
    WorkDim = 30,
    GlobalSize = 31,
    EnqueuedWorkgroupSize = 32,
    GlobalOffset = 33,
    GlobalLinearId = 34,
    SubgroupSize = 36,
    SubgroupMaxSize = 37,
    NumSubgroups = 38,
    NumEnqueuedSubgroups = 39,
    SubgroupId = 40,
    SubgroupLocalInvocationId = 41,
    VertexIndex = 42,
    InstanceIndex = 43,
    SubgroupEqMaskKHR = 4416,
    SubgroupGeMaskKHR = 4417,
    SubgroupGtMaskKHR = 4418,
    SubgroupLeMaskKHR = 4419,
    SubgroupLtMaskKHR = 4420,
    BaseVertex = 4424,
    BaseInstance = 4425,
    DrawIndex = 4426,
    DeviceIndex = 4438,
    ViewIndex = 4440,
    BaryCoordNoPerspAMD = 4992,
    BaryCoordNoPerspCentroidAMD = 4993,
    BaryCoordNoPerspSampleAMD = 4994,
    BaryCoordSmoothAMD = 4995,
    BaryCoordSmoothCentroidAMD = 4996,
    BaryCoordSmoothSampleAMD = 4997,
    BaryCoordPullModelAMD = 4998,
    FragStencilRefEXT = 5014,
    ViewportMaskNV = 5253,
    SecondaryPositionNV = 5257,
    SecondaryViewportMaskNV = 5258,
    PositionPerViewNV = 5261,
    ViewportMaskPerViewNV = 5262,
    FullyCoveredEXT = 5264,
}

#[derive(Debug,FromPrimitive,ToPrimitive)]
pub enum Capability{
    Matrix = 0,
    Shader = 1,
    Geometry = 2,
    Tessellation = 3,
    Addresses = 4,
    Linkage = 5,
    Kernel = 6,
    Vector16 = 7,
    Float16Buffer = 8,
    Float16 = 9,
    Float64 = 10,
    Int64 = 11,
    Int64Atomics = 12,
    ImageBasic = 13,
    ImageReadWrite = 14,
    ImageMipmap = 15,
    Pipes = 17,
    Groups = 18,
    DeviceEnqueue = 19,
    LiteralSampler = 20,
    AtomicStorage = 21,
    Int16 = 22,
    TessellationPointSize = 23,
    GeometryPointSize = 24,
    ImageGatherExtended = 25,
    StorageImageMultisample = 27,
    UniformBufferArrayDynamicIndexing = 28,
    SampledImageArrayDynamicIndexing = 29,
    StorageBufferArrayDynamicIndexing = 30,
    StorageImageArrayDynamicIndexing = 31,
    ClipDistance = 32,
    CullDistance = 33,
    ImageCubeArray = 34,
    SampleRateShading = 35,
    ImageRect = 36,
    SampledRect = 37,
    GenericPointer = 38,
    Int8 = 39,
    InputAttachment = 40,
    SparseResidency = 41,
    MinLod = 42,
    Sampled1D = 43,
    Image1D = 44,
    SampledCubeArray = 45,
    SampledBuffer = 46,
    ImageBuffer = 47,
    ImageMSArray = 48,
    StorageImageExtendedFormats = 49,
    ImageQuery = 50,
    DerivativeControl = 51,
    InterpolationFunction = 52,
    TransformFeedback = 53,
    GeometryStreams = 54,
    StorageImageReadWithoutFormat = 55,
    StorageImageWriteWithoutFormat = 56,
    MultiViewport = 57,
    SubgroupBallotKHR = 4423,
    DrawParameters = 4427,
    SubgroupVoteKHR = 4431,
    // Spec has somehow the same number for different variants.
    // I just removed the second variant, this might be wrong but I dont know
    // how to do it differently
    StorageBuffer16BitAccess = 4433,
    //StorageUniformBufferBlock16 = 4433,
    //Same problem here
    UniformAndStorageBuffer16BitAccess = 4434,
    //StorageUniform16 = 4434,
    StoragePushConstant16 = 4435,
    StorageInputOutput16 = 4436,
    DeviceGroup = 4437,
    MultiView = 4439,
    VariablePointersStorageBuffer = 4441,
    VariablePointers = 4442,
    AtomicStorageOps = 4445,
    SampleMaskPostDepthCoverage = 4447,
    ImageGatherBiasLodAMD = 5009,
    FragmentMaskAMD = 5010,
    StencilExportEXT = 5013,
    ImageReadWriteLodAMD = 5015,
    SampleMaskOverrideCoverageNV = 5249,
    GeometryShaderPassthroughNV = 5251,
    //And here
    ShaderViewportIndexLayerEXT = 5254,
    //ShaderViewportIndexLayerNV = 5254,
    ShaderViewportMaskNV = 5255,
    ShaderStereoViewNV = 5259,
    PerViewAttributesNV = 5260,
    FragmentFullyCoveredEXT = 5265,
    SubgroupShuffleINTEL = 5568,
    SubgroupBufferBlockIOINTEL = 5569,
    SubgroupImageBlockIOINTEL = 5570,
}
