#![allow(dead_code)]

extern crate nom;
#[macro_use]
extern crate num_derive;
extern crate num_traits;

pub mod parser;
pub mod spirv;

